# pandora-widget [![spm version](http://127.0.0.1:3000/badge/pandora-widget)](http://127.0.0.1:3000/package/pandora-widget)

---



## Install

```
$ spm install pandora-widget --save
```

## Usage

```js
var pandoraWidget = require('pandora-widget');
// use pandoraWidget
```
