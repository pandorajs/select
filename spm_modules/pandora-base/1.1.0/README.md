# pandora-base [![spm version](http://127.0.0.1:3000/badge/pandora-base)](http://127.0.0.1:3000/package/pandora-base)

---



## Install

```
$ spm install pandora-base --save
```

## Usage

```js
var pandoraBase = require('pandora-base');
// use pandoraBase
```
